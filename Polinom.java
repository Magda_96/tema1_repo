package tema1;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class Polinom {
	  ArrayList<Monom> monom1 = new ArrayList<Monom>();
	  
	public void adaugareMonom(String s){
			s = s.replaceAll("\\-","+-");						
			String[] re = s.split("[+]");
			String strCoeficient = new String("");
			String strExponent = new String("");
			int valoare1;
			int valoare2;			
			for (int z = 0; z < re.length; z++) {							
				if(re[z]=="x"){	
					strCoeficient = "1";
					strExponent = "1";
					
					
				}else {
					String[] res = re[z].split("[x^]");
				
				if(res.length==3){
					if(res[0].isEmpty()){
						strCoeficient = "1";
						strExponent = res[2];						
					}
					else{
						strCoeficient = res[0];
						strExponent = res[2];
					
					}
				}else
					if(res.length==1){	
						boolean found=false;
						for( int a=0; a<re[z].length();a++)
							if(re[z].charAt(a)=='x')found=true;
						if(found==true){
			
							strCoeficient = res[0];
							strExponent = "1"; 
						}
						else{	
							strCoeficient = res[0];
							strExponent = "0"; 
							
							
						}
					}						
				}
				valoare1 = Integer.parseInt(strCoeficient);
				valoare2 = Integer.parseInt(strExponent);				
				Monom m = new Monom(valoare1,valoare2);
				monom1.add(m);				
				}				
			}
	
	public Polinom aduna(Polinom p2){
		Polinom p1 = this;
		Polinom result = new Polinom();
		Polinom result1 = new Polinom();		
		for (Monom m : p1.monom1){
			Monom mono = new Monom(0,m.getExponent());
			for (Monom n : p2.monom1){
				if (n.getExponent() == m.getExponent()) mono.setCoeficient(n.getCoeficient());
			}
			mono.setCoeficient(m.getCoeficient()+mono.getCoeficient());
			result.monom1.add(mono);
		}		
		
		for (Monom n: p2.monom1){
			int k=0;
			for(Monom r: result.monom1){
				if (n.getExponent() == r.getExponent()) k++;
			}
			if (k ==0) result1.monom1.add(n);
		}		
		for (Monom z : result1.monom1){
			result.monom1.add(z);
		}		
		result.sorteaza();
		return result;
	}
	
	public Polinom scade(Polinom p2){
		Polinom p1 = this;
		Polinom result = new Polinom();
		Polinom result1 = new Polinom();		
		for (Monom m : p1.monom1){
			Monom mono = new Monom(0,m.getExponent());
			for (Monom n : p2.monom1){
				if (n.getExponent() == m.getExponent()) mono.setCoeficient(n.getCoeficient());
			}
			mono.setCoeficient(m.getCoeficient()-mono.getCoeficient());
			result.monom1.add(mono);
		}		
		for (Monom n: p2.monom1){
			int k=0;
			for(Monom r: result.monom1){
				if (n.getExponent() == r.getExponent()) k++;
			}
			Monom mono = new Monom(-n.getCoeficient(),n.getExponent());
			if (k ==0) result1.monom1.add(mono);
		}		
		for (Monom z : result1.monom1){
			result.monom1.add(z);
		}		
		result.sorteaza();
		return result;
	}	
	
	public Polinom inmulteste(Polinom p2){
		Polinom p1 = this;
		Polinom tot = new Polinom();
		Polinom result = new Polinom();
		int nr =0;
		int nr_exponent=0;
		for (Monom mon1 : p1.monom1)
		{
			for (Monom mon2 : p2.monom1){
				int coef = mon1.getCoeficient() * mon2.getCoeficient();
				int exp = mon1.getExponent() + mon2.getExponent();				
				Monom aux = new Monom(coef,exp);
				tot.monom1.add(aux);
				nr++;
				if (nr==1) nr_exponent = aux.getExponent();
			}
		}
		int exp = nr_exponent;		
		while (exp>=0){
			
			int k =0;
			int coef=0;
			for (Monom q : tot.monom1){
				if (q.getExponent() == exp){					
					k=1;
					coef = coef + q.getCoeficient();
				}				
			}
			if (k == 1){
				Monom mo = new Monom(coef,exp);
				result.monom1.add(mo);
			}
			exp--;			
		}		
		return result;
	}
	
	public Polinom imparte(Polinom p2, boolean var){
		Polinom p1 = this;
		p1.sorteaza();p2.sorteaza();
		Polinom result = new Polinom();
		Polinom rest = new Polinom();
		Polinom deimp = new Polinom();
		Monom mon2 = p2.monom1.get(0);
		deimp =p1;	
		int exp = p1.monom1.get(0).getExponent()+1;
	
		init_rest(rest,exp);
		set_rest(rest,exp,p1);	
		int expDeimp = deimp.monom1.get(0).getExponent();		
		while (expDeimp > mon2.getExponent()){	
			Monom m = new Monom(deimp.monom1.get(0).getCoeficient()/mon2.getCoeficient(), deimp.monom1.get(0).getExponent()- mon2.getExponent());
			Polinom p = new Polinom();
			p.monom1.add(m);			
			result.monom1.add(m);
			Polinom pol = new Polinom();
			pol= p.inmulteste(p2);				
			schimb_rest(rest,exp,pol);
			form_deimp(deimp,rest);			
			expDeimp = deimp.monom1.get(0).getExponent();
		}
		Monom m = new Monom(deimp.monom1.get(0).getCoeficient()/mon2.getCoeficient(), deimp.monom1.get(0).getExponent()- mon2.getExponent());
		Polinom p = new Polinom();
		p.monom1.add(m);	
		result.monom1.add(m);
		Polinom pol = new Polinom();
		pol= p.inmulteste(p2);				
		schimb_rest(rest,exp,pol);
		form_deimp(deimp,rest);			
		expDeimp = deimp.monom1.get(0).getExponent();
		rest.sorteaza();
		if (var) return result;
					else return rest;
	}
	
	public Polinom deriveaza(){
		Polinom p = this;
		Polinom d = new Polinom();
		for (Monom z: p.monom1){
			int coef = z.getCoeficient()*z.getExponent();
			int exp = z.getExponent()-1;
			if(exp != -1){
			Monom mon = new Monom(coef,exp);
			d.monom1.add(mon);
			}			 
		}
		return d;
	}
	
	public PolinomDouble integreaza(){
		Polinom p = this;
		PolinomDouble inte = new PolinomDouble();
		for (Monom z: p.monom1){
			double coef = z.getExponent()+1;
			coef=1/coef;
			coef=coef*z.getCoeficient();
			int exp = z.getExponent()+1;
			MonomDouble mon = new MonomDouble(coef,exp);
			inte.monoameDouble.add(mon);
		}
		return inte;
	}
	
	
	
	

	public void sorteaza(){
		Comparator<Monom> m = (mon1,mon2) -> Integer.compare(mon2.getExponent(),mon1.getExponent());
		this.monom1.sort(m);
	}
	
	public Polinom artificiu(){
		Polinom p = this;
		Polinom result = new Polinom();		
		for(Monom z: p.monom1){
			if (z.getCoeficient() != 0){
				Monom m = new Monom(z.getCoeficient(),z.getExponent());
				result.monom1.add(m);				
			}
		}		
		return result;		
	}

	public String getAll() {
		String all = "";
		for (Monom m : monom1) {
			DecimalFormat df = new DecimalFormat("##.##");
			df.setRoundingMode(RoundingMode.DOWN);
			if (m.getCoeficient()<0){
				if (m.getCoeficient()==-1){
					if (m.getExponent()==1)  all = all +"  -X ";					
					else if (m.getExponent() ==0) all = all +"  -1";
						else all = all +"  "+ df.format(m.getCoeficient()) + "-X^" + m.getExponent(); 
						}
			else if (m.getExponent()==1) all = all +"  "+ df.format(m.getCoeficient()) + "X";
			else if (m.getExponent()==0) all = all +"  "+ df.format(m.getCoeficient());
			else all = all +"  "+ df.format(m.getCoeficient()) + "X^" + m.getExponent();
			}else {
				if (m.getCoeficient()==1){
					if (m.getExponent()==1) all = all +"  +X ";	
					else if (m.getExponent()==0) 
					all = all +"  +1";					
						else all = all +"  +X^" + m.getExponent();
						}				
			else if (m.getExponent()==1) all = all +"  +"+ df.format(m.getCoeficient()) + "X";
			else if (m.getExponent()==0) all = all +"  +"+ df.format(m.getCoeficient());
			else all = all +"  +"+ df.format(m.getCoeficient()) + "X^" + m.getExponent();							
			}
		}
		return all;
	}

	public int dim() {
		return 0;
	}	
	private void init_rest(Polinom rest, int exp){
		for (int i = 0 ;i < exp; i++){
			Monom m= new Monom(0,i);
			rest.monom1.add(m);
		}	
	}
	private void set_rest(Polinom rest, int exp, Polinom p1){
		for (Monom m: p1.monom1){
			for(int i=0;i<exp;i++)
			if (m.getExponent()== i ){
				rest.monom1.get(i).setCoeficient(m.getCoeficient());
			}
		}	
	}
	private void schimb_rest(Polinom rest, int exp, Polinom pol){
		for (Monom t: pol.monom1){
			for(int i= 0;i<exp;i++)
			if (t.getExponent()== i ){
				int coef = rest.monom1.get(i).getCoeficient();
				rest.monom1.get(i).setCoeficient(coef-t.getCoeficient());
			}
		}
	}
	private void form_deimp(Polinom deimp, Polinom rest) {
		deimp.monom1.clear();
		for (Monom n: rest.monom1){
			if (n.getCoeficient()!=0) deimp.monom1.add(n);
		}
		deimp.sorteaza();
		
	}


}