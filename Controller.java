package tema1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller {
	private View v;
	
	public Controller(View v){
		this.v = v;
		v.showAdunareListener(new AddListenerAdunare());
		v.showScadereListener(new AddListenerScadere());
		v.showInmultireListener(new AddListenerInmultire());
		v.showImpartireListener(new AddListenerImpartire());
		v.showDerivareListener(new AddListenerDerivare());
		v.showIntegrareListener(new AddListenerIntegrare());
		v.clearListener(new AddListenerClear());
	}
	
	class AddListenerAdunare implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Polinom p1 = new Polinom();
			Polinom p2 = new Polinom();
			Polinom result = new Polinom();
			String pol1 = v.getPol1();
			String pol2 = v.getPol2();
			
			p1.adaugareMonom(pol1);
			p2.adaugareMonom(pol2);
			
			result = p1.aduna(p2);
			result = result.artificiu();
			String all = result.getAll();
			v.setAdunare(all);		
		}		
	}
	
	class AddListenerScadere implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Polinom p1 = new Polinom();
			Polinom p2 = new Polinom();
			Polinom result = new Polinom();
			String pol1 = v.getPol1();
			String pol2 = v.getPol2();
			String all= new String();
			
			p1.adaugareMonom(pol1);
			p2.adaugareMonom(pol2);
			
			result = p1.scade(p2);
			int i=result.dim();
			for (Monom m: result.monom1 )
				if ( m.getCoeficient()!=0) i--;
			result = result.artificiu();
			if (i==0) all="0";
			else all = result.getAll();
			v.setScadere(all);
		}
	}
	
	class AddListenerInmultire implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Polinom p1 = new Polinom();
			Polinom p2 = new Polinom();
			Polinom result = new Polinom();
			String pol1 = v.getPol1();
			String pol2 = v.getPol2();
			
			p1.adaugareMonom(pol1);
			p2.adaugareMonom(pol2);
			
			result = p1.inmulteste(p2);
			result = result.artificiu();
			String all = result.getAll();
			v.setInmultire(all);
		}
	}
	
	class AddListenerImpartire implements ActionListener{
		public void actionPerformed(ActionEvent e){			
			Polinom p1 = new Polinom();
			Polinom p2 = new Polinom();
			Polinom result = new Polinom();
			Polinom rest = new Polinom();
			String pol1 = v.getPol1();
			String pol2 = v.getPol2();
			
			
			p1.adaugareMonom(pol1);
			p2.adaugareMonom(pol2);
			result = p1.imparte(p2,true);
			rest = p1.imparte(p2,false);
			String all = result.getAll();
			String all2 = rest.getAll();
			v.setImpartire("Catul este: "+all+"\t iar restul este:"+all2);
		}
	}
	
	class AddListenerDerivare implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Polinom p2 = new Polinom();
			Polinom p1 = new Polinom();
			Polinom result2 = new Polinom();
			Polinom result1 = new Polinom();
			String pol1 = v.getPol1();
			String pol2 = v.getPol2();
			p1.adaugareMonom(pol1);
			p2.adaugareMonom(pol2);
			result1 = p1.deriveaza();
			result2 = p2.deriveaza();	
			result1.sorteaza();
			result2.sorteaza();			
			String all = result1.getAll();
			String all1 = result2.getAll();
			v.setDerivare1(all);
			v.setDerivare2(all1);
		}
	}
	class AddListenerIntegrare implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Polinom p1 = new Polinom();
			Polinom p2 = new Polinom();
			PolinomDouble result1 = new PolinomDouble();
			PolinomDouble result2 = new PolinomDouble();
			String pol1 = v.getPol1();
			String pol2 = v.getPol2();
			p1.adaugareMonom(pol1);
			p2.adaugareMonom(pol2);
			result1 = p1.integreaza();
			result2 = p2.integreaza();	
			result1.sorteaza();
			result2.sorteaza();
			
			String all = result1.getAll()+" +C";
			String all1 = result2.getAll() + " +C";
			v.setIntegrare1(all);
			v.setIntegrare2(all1);
		}
	}
	
	class AddListenerClear implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String s = "";
			v.setAdunare(s);
			v.setScadere(s);
			v.setImpartire(s);
			v.setInmultire(s);
			v.setDerivare1(s);
			v.setDerivare2(s);
			v.setIntegrare1(s);
			v.setIntegrare2(s);
	
		}
	}
}