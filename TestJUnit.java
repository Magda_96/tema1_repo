package tema1;

import org.junit.*;



public class TestJUnit {
	private Polinom polinom1 = new Polinom();
	private Polinom polinom2 = new Polinom();
	private Polinom polinom3 = new Polinom();
	private Polinom polinom4 = new Polinom();
	private PolinomDouble polinom31 = new PolinomDouble();
	private PolinomDouble polinom41 = new PolinomDouble();
	private String raspuns = new String("");
	private String raspuns1 = new String("");

	@Before
	public void set_date(){
		polinom1.adaugareMonom("3x^4+2x^2+5x+2");
		polinom2.adaugareMonom("x^2+3");
	}
	
	
	public void suma(){
		String s = new String();
		set_date();
		polinom3 = polinom1.aduna(polinom2);
		polinom3.artificiu();
		raspuns = polinom3.getAll();
		s = "  +3X^4  +3X^2  +5X  +5";
		assertEquals(raspuns,s);
	}
	
	@Test
	public void scadere(){
		String s = new String();
		set_date();
		polinom3 = polinom1.scade(polinom2);
		polinom3.artificiu();
		raspuns = polinom3.getAll();
		s = "  +3X^4  +X^2  +5X  -1";
		assertEquals(raspuns,s);
	}
	
	@Test
	public void inmultire(){
		String s = new String();
		set_date();
		polinom3 = polinom1.inmulteste(polinom2);
		polinom3.artificiu();
		raspuns = polinom3.getAll();
		s = "  +3X^6  +11X^4  +5X^3  +8X^2  +15X  +6";
		assertEquals(raspuns,s);
	}
	
	@Test
	public void impartire(){
		String s1 = new String();
		String s2 = new String();
		set_date();
		polinom3 = polinom1.imparte(polinom2,true);
		polinom4 = polinom1.imparte(polinom2,false);
		raspuns = polinom3.getAll();
		raspuns1 = polinom4.getAll();
		s1 = " +3x^2 -7";
		s2 = "  +0X  +23";
		assertEquals(raspuns,s1);
		assertEquals(raspuns1,s2);
	}
	
	@Test
	public void derivare(){
		String s1 = new String();
		String s2 = new String();
		set_date();
		polinom3 = polinom1.deriveaza();
		polinom4 = polinom2.deriveaza();
		polinom3.artificiu();
		raspuns = polinom3.getAll();
		raspuns1 = polinom4.getAll();
		s1 = "  +12X^3  +4X  +5";
		s2 = "  +2X";
		assertEquals(raspuns,s1);
		assertEquals(raspuns1,s2);		
	}
	@Test
	public void integrare(){
		String s1 = new String();
		String s2 = new String();
		set_date();
		polinom31 = polinom1.integreaza();
		polinom41 = polinom2.integreaza();
		raspuns = polinom31.getAll();
		raspuns1 = polinom41.getAll();
		s1 = "  +0.4X^5  +X^3  +2X^2  +X";
		s2 = "  +0.66X^3  +3X";
		assertEquals(raspuns,s1);
		assertEquals(raspuns1,s2);
		}
	
	
	private void assertEquals(String raspuns2, String s) {		
	}

}

