package tema1;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
public class PolinomDouble {
	ArrayList<MonomDouble> monoameDouble = new ArrayList<MonomDouble>();
	
	public void sorteaza(){
		Comparator<MonomDouble> m = (mon1,mon2) -> Integer.compare(mon2.getExponent(),mon1.getExponent());
		this.monoameDouble.sort(m);
	}
	public String getAll() {
		String all = "";
		for (MonomDouble m : monoameDouble) {
			DecimalFormat df = new DecimalFormat("##.##");
			df.setRoundingMode(RoundingMode.DOWN);
			if (m.getCoeficient()<0){
				if (m.getCoeficient()==-1){
					if (m.getExponent()==1)  all = all +"  -X ";					
					else if (m.getExponent() ==0) all = all +"  -1";
	
						else all = all +"  "+ df.format(m.getCoeficient()) + "-X^" + m.getExponent(); 
						}
			else if (m.getExponent()==1) all = all +"  "+ df.format(m.getCoeficient()) + "X";
			else if (m.getExponent()!=0) all = all +"  "+ df.format(m.getCoeficient()) + "X^" + m.getExponent();
			else 
				 all = all +"  "+ df.format(m.getCoeficient());
			}else {
				if (m.getCoeficient()==1){
					if (m.getExponent()!=1) all = all +"  +1";	
					else if (m.getExponent()==0) 
					
					all = all +"  +X ";	
						else all = all +"  +X^" + m.getExponent();
						}				
			else if (m.getExponent()==1) all = all +"  +"+ df.format(m.getCoeficient()) + "X";
			else if (m.getExponent()==0) all = all +"  +"+ df.format(m.getCoeficient());
			else all = all +"  +"+ df.format(m.getCoeficient()) + "X^" + m.getExponent();							
			}
		}
		return all;
	}	
	
}
