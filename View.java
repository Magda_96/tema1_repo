package tema1;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel = new JPanel();
	Image showImage = new ImageIcon(this.getClass().getResource("/images/save.png")).getImage();
	Image clearImage = new ImageIcon(this.getClass().getResource("/images/clear.png")).getImage();
	
	private static JLabel label = new JLabel();
	private static JLabel label1 = new JLabel();
	private static JLabel label2 = new JLabel();
	private static JLabel bigLabel = new JLabel();
	private static JLabel backgr = new JLabel();
	
	private static JLabel labelAdunare = new JLabel();
	private static JLabel labelScadere = new JLabel();
	private static JLabel labelInmultire = new JLabel();
	private static JLabel labelImpartire = new JLabel();
	private static JLabel labelDerivare = new JLabel();
	private static JLabel labelIntegrare = new JLabel();
	
	private static JButton showAdunare = new JButton("Show");
	private static JButton showScadere = new JButton("Show");
	private static JButton showInmultire = new JButton("Show");
	private static JButton showImpartire = new JButton("Show");
	private static JButton showDerivare = new JButton("Show");
	private static JButton showIntegrare = new JButton("Show");
	private static JButton clear = new JButton("");
	
	private static JTextField textP1 = new JTextField();
	private static JTextField textP2 = new JTextField();
	private static JTextField textAdunare = new JTextField(); 
	private static JTextField textScadere = new JTextField(); 
	private static JTextField textInmultire = new JTextField(); 
	private static JTextField textImpartire = new JTextField(); 
	private static JTextField textDerivare1 = new JTextField(); 
	private static JTextField textDerivare2 = new JTextField(); 
	private static JTextField textIntegrare1= new JTextField(); 
	private static JTextField textIntegrare2 = new JTextField(); 

	
	public View(){
		add(mainPanel);
		mainPanel.setLayout(null);
		addComponents();
		jFrameSetup();
		mainPanel.setBackground(Color.cyan);
				
	}
	
	private void jFrameSetup(){
		setTitle("Capros Magdalena");
		setSize(1150,700);
		backgr.setBounds(0, 0, 1150, 900);
		
	
		Image img1 = new ImageIcon(this.getClass().getResource("/6.jpg")).getImage();
		backgr.setIcon(new ImageIcon(img1));
		mainPanel.add(backgr);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 		
	}
	
	private void addComponents(){
		bigLabel.setText("Calculator de polinoame");
		bigLabel.setBounds(70, 20, 1000,150);
		bigLabel.setFont(new Font("Times", 30, 55));
		bigLabel.setHorizontalAlignment(SwingConstants.CENTER);
		mainPanel.add(bigLabel);		
		
		label.setFont(new Font("Times", 30, 30));
		mainPanel.add(label);		
		
		label1.setText("Polinom 1:");
		label1.setBounds(250, 160, 300,30);
		label1.setFont(new Font("Times", 30, 32));
		mainPanel.add(label1);
		
		label2.setText("Polinom 2:");
		label2.setBounds(250, 210, 350,30);		
		label2.setFont(new Font("Times", 30,32));
		mainPanel.add(label2);
		
		textP1.setBounds(450, 160, 300,50);
		textP1.setSize(450, 30);
		textP1.setFont(new Font("Times", 30,34));
		mainPanel.add(textP1);
				
		textP2.setBounds(450, 210, 300,30);
		textP2.setSize(450, 30);
		textP2.setFont(new Font("Times", 30,34));
		mainPanel.add(textP2);
		
		labelAdunare.setText("Adunare:");
		labelAdunare.setBounds(250, 280, 300,30);		
		labelAdunare.setFont(new Font("Times", 30,32));
		mainPanel.add(labelAdunare);

		labelScadere.setText("Scadere:");
		labelScadere.setBounds(250, 330, 300,30);		
		labelScadere.setFont(new Font("Times", 30,32));
		mainPanel.add(labelScadere);
		
		labelInmultire.setText("Inmultire:");
		labelInmultire.setBounds(250, 380, 300,30);		
		labelInmultire.setFont(new Font("Times", 30,32));
		mainPanel.add(labelInmultire);
		
		labelImpartire.setText("Impartire:");
		labelImpartire.setBounds(250, 430, 300,30);		
		labelImpartire.setFont(new Font("Times", 30,32));
		mainPanel.add(labelImpartire);
		
		labelDerivare.setText("Derivare:");
		labelDerivare.setBounds(250, 490, 300,30);		
		labelDerivare.setFont(new Font("Times", 30,32));
		mainPanel.add(labelDerivare);
		
		labelIntegrare.setText("Integrare:");
		labelIntegrare.setBounds(250, 565, 300,32);		
		labelIntegrare.setFont(new Font("Times", 30,32));
		mainPanel.add(labelIntegrare);
		
		clear.setBounds(1040, 100, 75, 540);
		clear.setVerticalTextPosition(AbstractButton.CENTER);
		clear.setIcon(new ImageIcon(clearImage));
		mainPanel.add(clear);
		
		showAdunare.setBounds(129, 280, 95, 30);
		showAdunare.setIcon(new ImageIcon(showImage));
		mainPanel.add(showAdunare);
		
		showScadere.setBounds(129, 330, 95, 30);
		showScadere.setIcon(new ImageIcon(showImage));
		mainPanel.add(showScadere);
		
		showInmultire.setBounds(129, 380, 95, 30);
		showInmultire.setIcon(new ImageIcon(showImage));
		mainPanel.add(showInmultire);
		
		showImpartire.setBounds(129, 430, 95, 30);
		showImpartire.setIcon(new ImageIcon(showImage));
		mainPanel.add(showImpartire);
		
		showDerivare.setBounds(129, 490, 95, 30);
		showDerivare.setIcon(new ImageIcon(showImage));
		mainPanel.add(showDerivare);
		
		showIntegrare.setBounds(129, 565, 95, 30);
		showIntegrare.setIcon(new ImageIcon(showImage));
		mainPanel.add(showIntegrare);
		
		
		textAdunare.setBounds(450, 280, 300,30);
		textAdunare.setSize(550, 30);
		mainPanel.add(textAdunare);
		
		textScadere.setBounds(450, 330, 300,30);
		textScadere.setSize(550, 30);
		mainPanel.add(textScadere);
		
		textInmultire.setBounds(450, 380, 300,30);
		textInmultire.setSize(550, 30);
		mainPanel.add(textInmultire);
		
		textImpartire.setBounds(450, 430, 300,30);
		textImpartire.setSize(550, 30);
		mainPanel.add(textImpartire);
		
		textDerivare1.setBounds(450, 480, 300,30);
		textDerivare1.setSize(550, 30);
		mainPanel.add(textDerivare1);
		
		textDerivare2.setBounds(450,520, 300,30);
		textDerivare2.setSize(550, 30);
		mainPanel.add(textDerivare2);
		
		textIntegrare1.setBounds(450, 570, 300,30);
		textIntegrare1.setSize(550, 30);
		mainPanel.add(textIntegrare1);
		
		textIntegrare2.setBounds(450, 610, 300,30);
		textIntegrare2.setSize(550, 30);
		mainPanel.add(textIntegrare2);
		
		
	}

	public void showAdunareListener(ActionListener a){
		showAdunare.addActionListener(a);
	}
	public void showScadereListener(ActionListener a){
		showScadere.addActionListener(a);
	}
	public void showInmultireListener(ActionListener a){
		showInmultire.addActionListener(a);
	}
	public void showImpartireListener(ActionListener a){
		showImpartire.addActionListener(a);
	}
	public void showDerivareListener(ActionListener a){
		showDerivare.addActionListener(a);
	}
	public void showIntegrareListener(ActionListener a){
		showIntegrare.addActionListener(a);
	}
	
	public void clearListener(ActionListener a){
		clear.addActionListener(a);
	}
	
	public void setAdunare(String s){
		textAdunare.setText(s);
		textAdunare.setFont(new Font("Times", 30,18));
	}
	
	public void setScadere(String s){
		textScadere.setText(s);
		textScadere.setFont(new Font("Times", 30,18));
	}
	
	public void setInmultire(String s){
		textInmultire.setText(s);
		textInmultire.setFont(new Font("Times", 30,18));
	}
	
	public void setImpartire(String s){
		textImpartire.setText(s);
		textImpartire.setFont(new Font("Times", 30,18));
	}
	
	public void setDerivare1(String s){
		textDerivare1.setText(s);
		textDerivare1.setFont(new Font("Times", 30,18));
	}
	
	public void setIntegrare1(String s){
		textIntegrare1.setText(s);
		textIntegrare1.setFont(new Font("Times", 30,18));
	}
	
	
	
	public void setDerivare2(String s){
		textDerivare2.setText(s);
		textDerivare2.setFont(new Font("Times", 30,18));
	}
	
	
	public void setIntegrare2(String s){
		textIntegrare2.setText(s);
		textIntegrare2.setFont(new Font("Times", 30,18));
	}
	
	
	public String getPol1(){
		return textP1.getText();
	}
	public String getPol2(){
		return textP2.getText();
	}
}

