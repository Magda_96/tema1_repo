package tema1;

public class Monom {
	private int coeficient;
	private int exponent;
	
	public Monom(int coeficient, int exponent){
			this.coeficient = coeficient;
			this.exponent = exponent;
		}
	
	public int getExponent(){
		return this.exponent;
	}
	
	public int getCoeficient(){
		return this.coeficient;
	}
	
	public void setExponent(int exponent){
		this.exponent = exponent;
	}
	
	public void setCoeficient(int coeficient){
		this.coeficient = coeficient;
	}
}

