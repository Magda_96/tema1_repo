package tema1;

public class MonomDouble{
	private double coeficient;
	private int exponent;
	
	public MonomDouble(double coeficient, int exponent){
			this.coeficient = coeficient;
			this.exponent = exponent;
	}
	
	public int getExponent(){
		return this.exponent;
	}
	
	public double getCoeficient(){
		return this.coeficient;
	}
	
	public void setExponent(int exponent){
		this.exponent = exponent;
	}
	
	public void setCoeficient(double coeficient){
		this.coeficient = coeficient;
	}

}
